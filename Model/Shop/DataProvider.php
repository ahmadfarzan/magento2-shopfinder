<?php 


namespace AhmadFarzan\ShopFinder\Model\Shop;

use AhmadFarzan\ShopFinder\Model\ResourceModel\Shop\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface; 
 
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {

	protected $collection;

	protected $dataPersistor;

	protected $loadedData;

	/**
	 * Constructor
	 * 
	 * @param string $name
	 * @param string $primaryFieldName
	 * @param string $requestFieldName
	 * @param CollectionFactory $blockCollectionFactory
	 * @param DataPersistorInterface $dataPersistor
	 * @param array $meta
	 * @param array $data
	 */
	public function __construct(
		$name,
		$primaryFieldName,
		$requestFieldName,
		CollectionFactory $collectionFactory,
		DataPersistorInterface $dataPersistor,
		array $meta = [],
		array $data = []
	)
	{
		$this->collection = $collectionFactory->create();
		$this->dataPersistor = $dataPersistor;
		parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
	}

	/**
	 * Get data
	 * 
	 * @return array
	 */
	public function getData()
	{
		if (isset($this->loadedData)) {
		    return $this->loadedData;
		}
		$items = $this->collection->getItems();
		foreach ($items as $model) {
            $modelData = $model->getData();
            $modelData['shop_image'] = unserialize($modelData['shop_image']);
		    $this->loadedData[$model->getId()] = $modelData;
		}
		$data = $this->dataPersistor->get('ahmadfarzan_shopfinder_shop');
		
		if (!empty($data)) {
		    $model = $this->collection->getNewEmptyItem();
		    $model->setData($data);
		    $this->loadedData[$model->getId()] = $model->getData();
		    $this->dataPersistor->clear('ahmadfarzan_shopfinder_shop');
		}
		
		return $this->loadedData;
	}
}
