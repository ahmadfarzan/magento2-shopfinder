<?php 


namespace AhmadFarzan\ShopFinder\Model;

use AhmadFarzan\ShopFinder\Api\Data\ShopInterface; 
 
class Shop extends \Magento\Framework\Model\AbstractModel implements ShopInterface {

	/**
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('AhmadFarzan\ShopFinder\Model\ResourceModel\Shop');
	}

	/**
	 * Get shop_id
	 * @return string
	 */
	public function getShopId()
	{
		return $this->getData(self::SHOP_ID);
	}

	/**
	 * Set shop_id
	 * @param string $shopId
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	public function setShopId($shopId)
	{
		return $this->setData(self::SHOP_ID,$shopId);
	}

	/**
	 * Get shop_name
	 * @return string
	 */
	public function getShopName()
	{
		return $this->getData(self::SHOP_NAME);
	}

	/**
	 * Set shop_name
	 * @param string $shop_name
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	public function setShopName($shop_name)
	{
		return $this->setData(self::SHOP_NAME,$shop_name);
	}

	/**
	 * Get shop_identifier
	 * @return string
	 */
	public function getShopIdentifier()
	{
		return $this->getData(self::SHOP_IDENTIFIER);
	}

	/**
	 * Set shop_identifier
	 * @param string $shop_identifier
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	public function setShopIdentifier($shop_identifier)
	{
		return $this->setData(self::SHOP_IDENTIFIER,$shop_identifier);
	}

	/**
	 * Get shop_storeviews
	 * @return string
	 */
	public function getShopStoreviews()
	{
		return $this->getData(self::SHOP_STOREVIEWS);
	}

	/**
	 * Set shop_storeviews
	 * @param string $shop_storeviews
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	public function setShopStoreviews($shop_storeviews)
	{
		return $this->setData(self::SHOP_STOREVIEWS,$shop_storeviews);
	}

	/**
	 * Get shop_country
	 * @return string
	 */
	public function getShopCountry()
	{
		return $this->getData(self::SHOP_COUNTRY);
	}

	/**
	 * Set shop_country
	 * @param string $shop_country
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	public function setShopCountry($shop_country)
	{
		return $this->setData(self::SHOP_COUNTRY,$shop_country);
	}

	/**
	 * Get shop_image
	 * @return string
	 */
	public function getShopImage()
	{
		return $this->getData(self::SHOP_IMAGE);
	}

	/**
	 * Set shop_image
	 * @param string $shop_image
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	public function setShopImage($shop_image)
	{
		return $this->setData(self::SHOP_IMAGE,$shop_image);
	}

	/**
	 * Get created_at
	 * @return string
	 */
	public function getCreatedAt()
	{
		return $this->getData(self::CREATED_AT);
	}

	/**
	 * Set created_at
	 * @param string $created_at
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	public function setCreatedAt($created_at)
	{
		return $this->setData(self::CREATED_AT,$created_at);
	}

	/**
	 * Get updated_at
	 * @return string
	 */
	public function getUpdatedAt()
	{
		return $this->getData(self::UPDATED_AT);
	}

	/**
	 * Set updated_at
	 * @param string $updated_at
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	public function setUpdatedAt($updated_at)
	{
		return $this->setData(self::UPDATED_AT,$updated_at);
	}
}
