<?php


    namespace AhmadFarzan\ShopFinder\Model\ResourceModel\Shop;


    class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
    {

        /**
         * Define resource model
         *
         * @return void
         */
        protected function _construct()
        {
            $this->_init(
                'AhmadFarzan\ShopFinder\Model\Shop',
                'AhmadFarzan\ShopFinder\Model\ResourceModel\Shop');
        }
    }
