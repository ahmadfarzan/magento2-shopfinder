<?php


    namespace AhmadFarzan\ShopFinder\Model\ResourceModel;


    class Shop extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
    {

        /**
         * Define resource model
         *
         * @return void
         */
        protected function _construct()
        {
            $this->_init('ahmadfarzan_shop', 'shop_id');
        }
    }
