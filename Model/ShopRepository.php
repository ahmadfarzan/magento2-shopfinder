<?php


    namespace AhmadFarzan\ShopFinder\Model;

    use Magento\Framework\Exception\NoSuchEntityException;
    use Magento\Framework\Exception\CouldNotSaveException;
    use AhmadFarzan\ShopFinder\Api\Data\ShopInterfaceFactory;
    use Magento\Store\Model\StoreManagerInterface;
    use AhmadFarzan\ShopFinder\Api\Data\ShopSearchResultsInterfaceFactory;
    use Magento\Framework\Exception\CouldNotDeleteException;
    use AhmadFarzan\ShopFinder\Api\ShopRepositoryInterface;
    use Magento\Framework\Reflection\DataObjectProcessor;
    use AhmadFarzan\ShopFinder\Model\ResourceModel\Shop as ResourceShop;
    use Magento\Framework\Api\SortOrder;
    use AhmadFarzan\ShopFinder\Model\ResourceModel\Shop\CollectionFactory as ShopCollectionFactory;
    use Magento\Framework\Api\DataObjectHelper;

    class ShopRepository implements ShopRepositoryInterface
    {

        protected $resource;

        private $storeManager;

        protected $dataObjectProcessor;

        protected $ShopCollectionFactory;

        protected $dataObjectHelper;

        protected $searchResultsFactory;

        protected $dataShopFactory;

        protected $ShopFactory;


        /**
         * @param ResourceShop $resource
         * @param ShopFactory $shopFactory
         * @param ShopInterfaceFactory $dataShopFactory
         * @param ShopCollectionFactory $shopCollectionFactory
         * @param ShopSearchResultsInterfaceFactory $searchResultsFactory
         * @param DataObjectHelper $dataObjectHelper
         * @param DataObjectProcessor $dataObjectProcessor
         * @param StoreManagerInterface $storeManager
         */
        public function __construct(
            ResourceShop $resource,
            ShopFactory $shopFactory,
            ShopInterfaceFactory $dataShopFactory,
            ShopCollectionFactory $shopCollectionFactory,
            ShopSearchResultsInterfaceFactory $searchResultsFactory,
            DataObjectHelper $dataObjectHelper,
            DataObjectProcessor $dataObjectProcessor,
            StoreManagerInterface $storeManager
        )
        {
            $this->resource = $resource;
            $this->shopFactory = $shopFactory;
            $this->shopCollectionFactory = $shopCollectionFactory;
            $this->searchResultsFactory = $searchResultsFactory;
            $this->dataObjectHelper = $dataObjectHelper;
            $this->dataShopFactory = $dataShopFactory;
            $this->dataObjectProcessor = $dataObjectProcessor;
            $this->storeManager = $storeManager;
        }

//        /**
//         * {@inheritdoc}
//         */
//        public function save(
//            \AhmadFarzan\ShopFinder\Api\Data\ShopInterface $shop
//        )
//        {
//            /* if (empty($shop->getStoreId())) {
//                $storeId = $this->storeManager->getStore()->getId();
//                $shop->setStoreId($storeId);
//            } */
//            try {
//                $this->resource->save($shop);
//            } catch (\Exception $exception) {
//                throw new CouldNotSaveException(__(
//                    'Could not save the shop: %1',
//                    $exception->getMessage()
//                ));
//            }
//            return $shop;
//        }
//
//        /**
//         * {@inheritdoc}
//         */
//        public function getById($shopId)
//        {
//            $shop = $this->shopFactory->create();
//            $shop->load($shopId);
//            if (!$shop->getId()) {
//                throw new NoSuchEntityException(__('Shop with id "%1" does not exist.', $shopId));
//            }
//            return $shop;
//        }

        /**
         * {@inheritdoc}
         */
        public function getList(
            \Magento\Framework\Api\SearchCriteriaInterface $criteria = NULL
        )
        {
            $currentStore = $this->storeManager->getStore()->getId();
            $items = [];

            $searchResults = $this->searchResultsFactory->create();
            $collection = $this->shopCollectionFactory->create();
            if ($currentStore != 1) {
                $collection->addFieldToFilter('shop_storeviews',
                    [
                        ['like' => '%,0,%'],
                        ['like' => '%,' . $currentStore . ',%']
                    ]
                );
            }

            if($criteria != NULL) {
                $searchResults->setSearchCriteria($criteria);
                foreach ($criteria->getFilterGroups() as $filterGroup) {
                    foreach ($filterGroup->getFilters() as $filter) {
                        $condition = $filter->getConditionType() ?: 'eq';
                        $fieldName = ltrim($filter->getField(), 'shop_');
                        switch ($fieldName) {
                            case 'name':
                            case 'identifier':
                            case 'country':
                                $fieldName = 'shop_' . $fieldName;
                                break;
                        }
                        switch ($filter->getConditionType()) {
                            case 'like':
                                $filter->setValue('%'.$filter->getValue().'%');
                                break;
                        }
                        $collection->addFieldToFilter($fieldName, [$condition => $filter->getValue()]);
                    }
                }
                $sortOrders = $criteria->getSortOrders();
                if ($sortOrders) {
                    /** @var SortOrder $sortOrder */
                    foreach ($sortOrders as $sortOrder) {
                        $collection->addOrder(
                            $sortOrder->getField(),
                            ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                        );
                    }
                }
                $collection->setCurPage($criteria->getCurrentPage());
                $collection->setPageSize($criteria->getPageSize());
            }
            foreach ($collection as $shopModel) {
                $shopData = $this->dataShopFactory->create();
                $shopValues = $shopModel->getData();
                $storeViews = trim($shopValues['shop_storeviews'], ",");
                $storeViews = explode(",", $storeViews);
                $shopValues['shop_storeviews'] = [];
                foreach ($storeViews as $storeView) {
                    if ($storeView == 0) {
                        $shopValues['shop_storeviews'][] = 'all';
                    } else {
                        $shopValues['shop_storeviews'][] = $this->storeManager->getStore($storeView)->getCode();
                    }

                }
                $shopValues['shop_image'] = unserialize($shopValues['shop_image']);
                $this->dataObjectHelper->populateWithArray(
                    $shopData,
                    $shopValues,
                    'AhmadFarzan\ShopFinder\Api\Data\ShopInterface'
                );
                $items[] = $this->dataObjectProcessor->buildOutputDataArray(
                    $shopData,
                    'AhmadFarzan\ShopFinder\Api\Data\ShopInterface'
                );
            }
            $searchResults->setTotalCount($collection->getSize());
            $searchResults->setItems($items);
            return $searchResults;
            $searchResults->setTotalCount($collection->getSize());
        }

//        /**
//         * {@inheritdoc}
//         */
//        public function delete(
//            \AhmadFarzan\ShopFinder\Api\Data\ShopInterface $shop
//        )
//        {
//            try {
//                $this->resource->delete($shop);
//            } catch (\Exception $exception) {
//                throw new CouldNotDeleteException(__(
//                    'Could not delete the Shop: %1',
//                    $exception->getMessage()
//                ));
//            }
//            return true;
//        }
//
//        /**
//         * {@inheritdoc}
//         */
//        public function deleteById($shopId)
//        {
//            return $this->delete($this->getById($shopId));
//        }
    }
