<?php 


namespace AhmadFarzan\ShopFinder\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface; 
 
class InstallSchema implements InstallSchemaInterface {

	/**
	 * {@inheritdoc}
	 */
	public function install(
		SchemaSetupInterface $setup,
		ModuleContextInterface $context
	)
	{
		$installer = $setup;
		$installer->startSetup();

		$table_ahmadfarzan_shop = $setup->getConnection()->newTable($setup->getTable('ahmadfarzan_shop'));

		$table_ahmadfarzan_shop->addColumn(
		  'shop_id',
		  \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
		  null,
		  array('identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,),
		  'Entity ID'
		);

		$table_ahmadfarzan_shop->addColumn(
		  'shop_name',
		  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		  255,
		  ['nullable' => false],
		  'Shop Name'
		);

		$table_ahmadfarzan_shop->addColumn(
		  'shop_identifier',
		  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		  255,
		  ['nullable' => true],
		  'Shop Identifier'
		);

		$table_ahmadfarzan_shop->addColumn(
		  'shop_storeviews',
		  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		  512,
		  ['nullable' => false],
		  'Shop Storeviews'
		);

		$table_ahmadfarzan_shop->addColumn(
		  'shop_country',
		  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
		  ['nullable' => false],
		  'Shop Country'
		);

		$table_ahmadfarzan_shop->addColumn(
		  'shop_image',
		  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		  5120,
		  ['nullable' => true],
		  'Shop Image'
		);

		$table_ahmadfarzan_shop->addColumn(
		  'created_at',
		  \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
		  null,
		  ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
		  'Created At'
		);

		$table_ahmadfarzan_shop->addColumn(
		  'updated_at',
		  \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
		  null,
		  ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
		  'Updated At'
		);

		$setup->getConnection()->createTable($table_ahmadfarzan_shop);

		$setup->endSetup();
	}
}
