<?php
    namespace AhmadFarzan\ShopFinder\Plugin;

    class AbstractDbPlugin
    {
        public function aroundAddFieldToFilter(\Magento\Framework\Data\Collection\AbstractDb $subject, \Closure $proceed, $field, $condition = null)
        {
            if ($field == 'shop_storeviews' AND count($condition) < 2) {
                $newCondition = [
                    ['like' => '%,0,%'],
                    ['like' => '%,'.$condition['eq'].',%'],
                ];
                $condition = $newCondition;
            }

            $return = $proceed($field, $condition);
            return $return;
        }
    }