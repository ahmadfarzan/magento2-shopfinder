<?php 


namespace AhmadFarzan\ShopFinder\Api;

use Magento\Framework\Api\SearchCriteriaInterface; 
 
interface ShopRepositoryInterface {


//	/**
//	 * Save Shop
//	 * @param \AhmadFarzan\ShopFinder\Api\Data\ShopInterface $shop
//	 * @return \AhmadFarzan\ShopFinder\Api\Data\ShopInterface
//	 * @throws \Magento\Framework\Exception\LocalizedException
//	 */
//
//	public function save(
//		\AhmadFarzan\ShopFinder\Api\Data\ShopInterface $shop
//	);
//
//	/**
//	 * Retrieve Shop
//	 * @param string $shopId
//	 * @return \AhmadFarzan\ShopFinder\Api\Data\ShopInterface
//	 * @throws \Magento\Framework\Exception\LocalizedException
//	 */
//
//	public function getById($shopId);

	/**
	 * Retrieve Shop matching the specified criteria.
	 * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
	 * @return \AhmadFarzan\ShopFinder\Api\Data\ShopSearchResultsInterface
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */

    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = NULL
    );

//    /**
//	 * Delete Shop
//	 * @param \AhmadFarzan\ShopFinder\Api\Data\ShopInterface $shop
//	 * @return bool true on success
//	 * @throws \Magento\Framework\Exception\LocalizedException
//	 */
//
//	public function delete(
//		\AhmadFarzan\ShopFinder\Api\Data\ShopInterface $shop
//	);
//
//	/**
//	 * Delete Shop by ID
//	 * @param string $shopId
//	 * @return bool true on success
//	 * @throws \Magento\Framework\Exception\NoSuchEntityException
//	 * @throws \Magento\Framework\Exception\LocalizedException
//	 */
//
//	public function deleteById($shopId);
}
