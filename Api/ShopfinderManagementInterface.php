<?php 


namespace AhmadFarzan\ShopFinder\Api;
 
 
interface ShopfinderManagementInterface {


	/**
	 * GET for shopfinder api
	 * @param string $param
	 * @return string
	 */
	
	public function getShopfinder($param);
}
