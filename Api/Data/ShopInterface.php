<?php 


namespace AhmadFarzan\ShopFinder\Api\Data;
 
 
interface ShopInterface {

	const UPDATED_AT = 'updated_at';
	const SHOP_NAME = 'shop_name';
	const CREATED_AT = 'created_at';
	const SHOP_COUNTRY = 'shop_country';
	const SHOP_IDENTIFIER = 'shop_identifier';
	const SHOP_STOREVIEWS = 'shop_storeviews';
	const SHOP_IMAGE = 'shop_image';
	const SHOP_ID = 'shop_id';


	/**
	 * Get shop_id
	 * @return string|null
	 */
	
	public function getShopId();

	/**
	 * Set shop_id
	 * @param string $shop_id
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	
	public function setShopId($shopId);

	/**
	 * Get shop_name
	 * @return string|null
	 */
	
	public function getShopName();

	/**
	 * Set shop_name
	 * @param string $shop_name
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	
	public function setShopName($shop_name);

	/**
	 * Get shop_identifier
	 * @return string|null
	 */
	
	public function getShopIdentifier();

	/**
	 * Set shop_identifier
	 * @param string $shop_identifier
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	
	public function setShopIdentifier($shop_identifier);

	/**
	 * Get shop_storeviews
	 * @return string|null
	 */
	
	public function getShopStoreviews();

	/**
	 * Set shop_storeviews
	 * @param string $shop_storeviews
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	
	public function setShopStoreviews($shop_storeviews);

	/**
	 * Get shop_country
	 * @return string|null
	 */
	
	public function getShopCountry();

	/**
	 * Set shop_country
	 * @param string $shop_country
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	
	public function setShopCountry($shop_country);

	/**
	 * Get shop_image
	 * @return string|null
	 */
	
	public function getShopImage();

	/**
	 * Set shop_image
	 * @param string $shop_image
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	
	public function setShopImage($shop_image);

	/**
	 * Get created_at
	 * @return string|null
	 */
	
	public function getCreatedAt();

	/**
	 * Set created_at
	 * @param string $created_at
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	
	public function setCreatedAt($created_at);

	/**
	 * Get updated_at
	 * @return string|null
	 */
	
	public function getUpdatedAt();

	/**
	 * Set updated_at
	 * @param string $updated_at
	 * @return AhmadFarzan\ShopFinder\Api\Data\ShopInterface
	 */
	
	public function setUpdatedAt($updated_at);
}
