<?php 


namespace AhmadFarzan\ShopFinder\Api\Data;
 
 
interface ShopSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface {


	/**
	 * Get Shop list.
	 * @return \AhmadFarzan\ShopFinder\Api\Data\ShopInterface[]
	 */
	
	public function getItems();

	/**
	 * Set shop_name list.
	 * @param \AhmadFarzan\ShopFinder\Api\Data\ShopInterface[] $items
	 * @return $this
	 */
	
	public function setItems(array $items);
}
