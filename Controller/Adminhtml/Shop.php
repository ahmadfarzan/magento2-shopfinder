<?php 


namespace AhmadFarzan\ShopFinder\Controller\Adminhtml;
 
 
abstract class Shop extends \Magento\Backend\App\Action {

	const ADMIN_RESOURCE = 'AhmadFarzan_ShopFinder::top_level';
	protected $_coreRegistry;

	/**
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Magento\Framework\Registry $coreRegistry
	 */
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\Registry $coreRegistry
	)
	{
		$this->_coreRegistry = $coreRegistry;
		parent::__construct($context);
	}

	/**
	 * Init page
	 * 
	 * @param \Magento\Backend\Model\View\Result\Page $resultPage
	 */
	public function initPage($resultPage)
	{
		$resultPage->setActiveMenu('AhmadFarzan::ahmadfarzan_shop')
		    ->addBreadcrumb(__('AhmadFarzan'), __('AhmadFarzan'))
		    ->addBreadcrumb(__('Manage Shops'), __('Manage Shops'));
		return $resultPage;
	}
}
